package com.optravis.lists

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleLongProperty
import javafx.concurrent.Task
import tornadofx.*
import kotlin.random.Random


fun main(args: Array<String>) {
    launch<MyApp>(args)
}

class MyApp: App(MyView::class)

class MyView: View() {

    val myController: MyController by inject()
    val listASize = SimpleIntegerProperty()
    val listBSize = SimpleIntegerProperty()
    val iterrateOverSmallerList = SimpleBooleanProperty()
    val resultTime = SimpleLongProperty()
    val resultSize = SimpleIntegerProperty()
    lateinit var runningTask: Task<Result>

    override val root = hbox {
        form {
            fieldset {
                field("List A size") {
                    textfield(listASize)
                }
                field("List B size") {
                    textfield(listBSize)
                }
                checkbox("Itterate over smaller list", iterrateOverSmallerList)
                button("Run") {
                    action {
                         runningTask = runAsync {
                            myController.intersect(listASize.get(), listBSize.get(), iterrateOverSmallerList.get())
                        } ui { result ->
                            resultSize.value = result.size
                            resultTime.value = result.timeMs
                        }
                    }
                }
                button("Cancel") {
                    action {
                        if(::runningTask.isInitialized) {
                            runningTask.cancel()
                        }
                        resultSize.value = 0
                        resultTime.value = 0
                    }
                }
                field("Result time (ms)") {
                    textfield(resultTime)
                }
                field("Result size") {
                    textfield(resultSize)
                }
            }
        }
    }
}


class MyController: Controller() {

    fun intersect(sizeA: Int, sizeB: Int, iterateOverSmallerList: Boolean): Result {
        val random = Random.Default
        val listA = (1..sizeA).map { random.nextInt() }
        val listB = (1..sizeB).map { random.nextInt() }

        val start: Long = System.currentTimeMillis()

        val result = if(listA.size > listB.size) {
            if(iterateOverSmallerList) {
                intersect(listB, listA)
            }
            else {
                intersect(listA, listB)
            }
        }
        else {
            if(iterateOverSmallerList) {
                intersect(listA, listB)
            }
            else {
                intersect(listB, listA)
            }
        }
        val end: Long = System.currentTimeMillis()
        return Result(result.size, end - start)


    }

    fun intersect(itt: List<Int>, convertToSet: List<Int>): Set<Int> {
        val hashSet = convertToSet.toHashSet()
        return itt.filterTo(HashSet()) { hashSet.contains(it) }
    }
}

class Result(
    val size: Int,
    val timeMs: Long
)